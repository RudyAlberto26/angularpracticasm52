import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {

  @Input() mensaje: any;
  @Output() Mensaje = new EventEmitter<number>();

  constructor() { }

  ngOnInit() {
  }

  onClick() {
    this.Mensaje.emit(this.mensaje.id);
  }

}
